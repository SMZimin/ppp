// var32.cpp : Defines the entry point for the console application.
// code down
#define _CRT_SECURE_NO_WARNINGS
#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <windows.h>
#include <omp.h>

const int MaxN = 1700;
const float Eps = 1e-5;

typedef  struct point {
	float x;
	float y;
} MasPnt;

typedef float BMasPnt[MaxN * 2];
typedef long int BMasSst[MaxN * 2];

MasPnt PrM[2][MaxN];
long int N;
float Res;
BMasPnt Ox, Oy;
BMasSst FOy;

bool More(float a, float b)
{
	return a - b > Eps;
}

void Swap(float &a, float &b)
{
	double t;
	t = a; 
	a = b; 
	b = t;
}

void SwapInt(long int &a,long int &b)
{
	long int t;
	t = a;
	a = b;
	b = t;
}

bool Eq(float a, float b)
{
	return fabs(a - b) < Eps;
}

void Sort(BMasPnt Ox, long int lf, long int rg)
{
	long int i, j;
	float x;
	i = lf;
	j = rg;
	x = Ox[(lf + rg) / 2]; // ���� div 2

	do
	{//Repeat
		while (More(x, Ox[i]))
		{
			i++;
		};
		while (More(Ox[j], x))
		{
			j--;
		};
		if (i <= j)
		{
			Swap(Ox[i], Ox[j]);
			i++;
			j--;
		}
	} 
	while (j >= i);//(j >= i); // Until i > j;
	if (lf < j)
	{
		Sort(Ox, lf, j);
	}
	if (i < rg)
	{
		Sort(Ox, i, rg);
	}
}
void FstSort(BMasPnt Ox, BMasSst SOx,long int lf, long int rg)
{
	long int i, j;
	float x;
	i = lf;
	j = rg;
	x = Ox[(lf + rg) / 2]; // ���� div 2

	do
	{//Repeat
		while (More(x, Ox[i]))
		{
			i++;
		};
		while (More(Ox[j], x))
		{
			j--;
		};
		if (i <= j)
		{
			Swap(Ox[i], Ox[j]);
			SwapInt(SOx[i], SOx[j]);
			i++;
			j--;
		}
	} while (j >= i); // Until i > j;
	if (lf < j)
	{
		FstSort(Ox, SOx, lf, j);
	}
	if (i < rg)
	{
		FstSort(Ox, SOx, i, rg);
	}
};


void Init()
{
	FILE *f;
	float x1, x2, y1, y2;
	f = fopen("D:\\�����\\������������\\���������� ������\\var32\\Debug\\inputMax.txt", "r");

	Res = 0;
	fscanf(f, "%d", &N);
	for (int i = 0; i < N; i++)
	{
		fscanf(f, "%f", &x1);
		fscanf(f, "%f", &y1);
		fscanf(f, "%f", &x2);
		fscanf(f, "%f", &y2);
		PrM[0][i].x = x1;	 PrM[0][i].y = y1;	 
		PrM[1][i].x = x2;	 PrM[1][i].y = y2;

		if (More(PrM[0][i].x, PrM[1][i].x))
			Swap(PrM[0][i].x, PrM[1][i].x);

		//If More(PrM[1, i].Y, PrM[2, i].Y)
		//	Then Swap(PrM[1, i].Y, PrM[2, i].Y);
		if (More(PrM[0][i].y, PrM[1][i].y))
			Swap(PrM[0][i].y, PrM[1][i].y);

		Ox[i * 2] = PrM[0][i].x;
		Ox[i * 2 + 1] = PrM[1][i].x;
	}

	fclose(f);
}

bool Peres(long int k,float x)
{
	return !More(PrM[0][k].x, x) && More(PrM[1][k].x, x);
};
 
float Calc(BMasPnt Ox, BMasSst SOx, long int N)
{
	float sc;
	long int bb;
	sc = 0; bb = 0;

	if (SOx[0] > 0)
	{
		bb++;
	};
	int i = 1;
#pragma omp parallel shared(bb,Ox) private(i)
{
#pragma omp for
	for (i = 1; i < N; i++) //For i : = 2 To N Do 
	{
		if (bb > 0)
		{

			sc += (Ox[i] - Ox[i - 1]);

			//�������� �������� �������� ����������� ����������
		}

		bb += SOx[i];
	}
}
	return sc;
}

float Change(float x,float rs)
{
	long int M = 0;

	int i = 0;
#pragma omp parallel for shared(Oy,FOy) private (i)
	for (i = 0; i < MaxN * 2; i++)
	{
		Oy[i] = 0;
		FOy[i] = 0;
	}
	//FillChar(Oy, SizeOf(Oy), 0);
	//FillChar(FOy, SizeOf(Foy), 0);
	i = 0;
#pragma omp parallel shared(FOy,rs,x,M,PrM,N) private (i)
	{
#pragma omp for
		for (i = 0; i < N; i++)
		{
			if (Peres(i, x))
			{
				Oy[M + 0] = PrM[0][i].y;
				Oy[M + 1] = PrM[1][i].y;

				FOy[M + 0] = 1;
				FOy[M + 1] = -1;
#pragma omp atomic
				M += 2;
			}

			if (M == 0)
			{
				rs = 0;
			}
			else
			{
#pragma omp critical
				{
					FstSort(Oy, FOy, 0, M - 1);
				}
				rs = fabs(Calc(Oy, FOy, M));
			
				//��������� Oy, ����������� ������������ ��������������� �������� ������� FOy; 
				//��������� ����� �������� ����� ������� � ������� Calc
			}
		}
	}
	
	
	return rs;
}

void Solve()
{
	

	Sort(Ox, 0, N * 2 - 1); //��������� �� ���������� �������� ���������� X ���������������
	Res = 0; //m � ����� �������, Res � �������� ������� ����������� ���������������
	float m = 0;
	int i = 0; 


		for (i = 0; i < N * 2; i++)
		{
			if (i != 0)
			{
				Res = Res + fabs((Ox[i] - Ox[i - 1])*m); // ���������� ������� ���������� �������
			}

			if ((i == 0) || !(Eq(Ox[i], Ox[i - 1])))
			{
				m = Change(Ox[i], m); //���������� ����� �������� ����� �������
			}
		}
	
}
int main()
{
	int start = GetTickCount();
	FILE *f;
	omp_set_num_threads(8);
	Init();

	Solve();
	
	f = fopen("D:\\�����\\������������\\���������� ������\\var32 � OpenMP\\Debug\\output.txt", "w");
	fprintf(f,"%f", Res);
	fclose(f);
	int end = GetTickCount();
	f = fopen("D:\\�����\\������������\\���������� ������\\var32 � OpenMP\\Debug\\time.txt", "w");
	fprintf(f, "%d", end - start);
	fclose(f);
	

    return 0;
}

